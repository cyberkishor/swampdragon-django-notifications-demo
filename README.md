swampdragon-django-notifications-demo
=====================================

Example code for implementing Django real-time notifications using SwampDragon.

To accompany the blog post at 
1) http://kishorkumarmahato.com.np/weekend-django-real-time-notifications-with-swampdragon/

2) http://wildfish.com/blog/2014/12/09/swampdragon-real-time-django-notifications/


Quickstart
----------

    pip install -r requirements.txt
    install redis server ([http://redis.io/topics/quickstart])

   
then:

    python manage.py runserver
    ptyhon manage.py runsd / python server.py 

    redis-server

Admin Credentials
-----------------

    user: admin
    pass: admin